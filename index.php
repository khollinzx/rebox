<?php

$pageTitle = "Rebox.ng";
$linkTitle = "home";
require_once($_SERVER["DOCUMENT_ROOT"] . "/constant/config.php");

require_once(ROOT_PATH . 'core/init.php');

include(ROOT_PATH . 'inc/landingPageheader.php');

include(ROOT_PATH . 'inc/landingPageNavbar.php');
?>

<!-- Header -->
<header id="header" class="header">
    <div class="header-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center " id="welcome-text">
                    <div class="text-container ">
                        <h1 style="text-align: center;color: #fff"><span class="turquoise">Rebox.Ng</span> is a centralized investment platform that make all investment option readily available for you</h1>
                        <p class="p-large" style="font-size: 18px; color: #fff;text-align: center;">With as little as <b>N</b>1000, <i class="fas fa-dollar-sign"></i>20, 50<i class="fas fa-euro-sign"></i>, 50<i class="fas fa-pound-sign"></i> you could own shares, money market instrucment, or even part of a project to be executed.</p>
                        <div class="text-center">
                            <a href="<?php echo BASE_URL; ?>login.php" class=" page-scroll btn-solid-reg mr-3 mb-3"> SIGN UP TO GET STARTED <i class="fa fa-right-arrow"></i> </a>
                        </div>
                        <p style="color:#fff;" class="testimonial-text">"<b>AFRICA</b> will define the future" - <b>Twitter</b> Chief Executive Jack Dorsey.</p>
                    </div> <!-- end of text-container -->
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of header-content -->
</header> <!-- end of header -->
<!-- end of header -->


<!-- Services -->

<div class="basic-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 text-center">
                <h2>INVEST WITH CONFIDENCE IN AFRICA'S BIGGEST FINANCIAL MARKET</h2>
                <p>GET ACCESS AND EASILY COMPARE INVESTMENT OPTION IN.</p>
            </div> <!-- end of col -->
        </div> <!-- end of row -->
        <div class="row" style="margin-top:-3%;">
            <div class="col-md-6 col-lg-6">
                <ul class="list-unstyled li-space-lg" style="font-size:20px;">
                    <li class="media box-item">
                        <span class="fa-stack">
                            <i style="color:#0377b6e0;" class="fas fa-circle fa-stack-2x"></i>
                            <i style="color:#fff;" class="fas fa-chart-line fa-stack-1x"></i>
                        </span>
                        <div class="media-body"> Shares.</div>
                    </li>
                    <li class="media box-item">
                        <span class="fa-stack">
                            <i style="color:coral;" class="fas fa-circle fa-stack-2x"></i>
                            <i style="color:#fff;" class="fas fa-credit-card fa-stack-1x"></i>
                        </span>
                        <div class="media-body">Trust Funds.</div>
                    </li>
                    <li class="media box-item">
                        <span class="fa-stack">
                            <i style="color:violet;" class="fas fa-circle fa-stack-2x"></i>
                            <i style="color:#fff;" class="fas fa-briefcase fa-stack-1x"></i>
                        </span>
                        <div class="media-body">Project Finance.</div>
                    </li>
                    <li class="media box-item">
                        <span class="fa-stack">
                            <i style="color:teal" class="fas fa-circle fa-stack-2x"></i>
                            <i style="color:#fff;" class="fas fa-chart-bar fa-stack-1x"></i>
                        </span>
                        <div class="media-body">Forex</div>
                    </li>
                </ul>
            </div> <!-- end of col -->

            <div class="col-md-6 col-lg-6">
                <ul class="list-unstyled li-space-lg" style="font-size:20px;">
                    <li class="media box-item">
                        <span class="fa-stack">
                            <i style="color:chocolate" class="fas fa-circle fa-stack-2x"></i>
                            <i style="color:#fff; margin-top: -25%;" class="fab fa-btc fa-stack-1x"></i>
                        </span>
                        <div class="media-body"> Cryptocurrencies.</div>
                    </li>
                    <li class="media box-item">
                        <span class="fa-stack">
                            <i style="color:goldenrod;" class="fas fa-circle fa-stack-2x"></i>
                            <i style="color:#fff;" class="fas fa-building fa-stack-1x"></i>
                        </span>
                        <div class="media-body">Real Estate.</div>
                    </li>
                    <li class="media box-item">
                        <span class="fa-stack">
                            <i style="color:orange;" class="fas fa-circle fa-stack-2x"></i>
                            <i style="color:#fff;" class="fas fa-wallet fa-stack-1x"></i>
                        </span>
                        <div class="media-body">Mutual Funds.</div>
                    </li>
                    <li class="media box-item">
                        <span class="fa-stack">
                            <i style="color:saddlebrown;" class="fas fa-circle fa-stack-2x"></i>
                            <i style="color:#fff;" class="fas fa-list-ul fa-stack-1x"></i>
                        </span>
                        <div class="media-body">And more investment options</div>
                    </li>
                </ul>
            </div> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of container -->
</div>


<div class="cards-1" style="margin-top: -4%;">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="image-container">
                    <img class="img-fluid slide_up" src="<?php echo BASE_URL; ?>assets/images/Investment.png" alt="alternative">
                </div> <!-- end of image-container -->
            </div>
        </div> <!-- end of col -->
    </div> <!-- end of container -->
</div> <!-- end of basic-1 -->
<!-- end of details 1 -->


<!-- Details 2 -->
<div class="basic-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="text-container text-center">
                    <h3>WHY INVEST IN US, EUROPE WHEN THE NEXT BIG MARKET IS HERE</h3>
                    <p>We make investment Accessible/ Inclusive by giving you the BEST investment optionfrom every sector.</p>
                    <ul class="list-unstyled li-space-lg ">
                        <li class="media text-center">
                            <i class="fas fa-check"></i>
                            <div class="media-body">you can own local shares, trust funds, finance project, trade forex all with this tiny box</div>
                        </li>
                        <li class="media">
                            <i class="fas fa-check"></i>
                            <div class="media-body">Invest directly into Africa from anywhere in the world</div>
                        <li class="media">
                            <i class="fas fa-check"></i>
                            <div class="media-body">Never miss a new opportunity to invest and make money</div>
                        </li>
                        <li class="media">
                            <i class="fas fa-check"></i>
                            <div class="media-body">Wether you have short term investment place or long term, Rebox got you covered</div>
                        </li>
                    </ul>
                </div> <!-- end of text-container -->
            </div> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of container -->
</div> <!-- end of basic-2 -->
<!-- end of details 2 -->

<div class="cards-1">
    <div class="container">
    </div> <!-- end of row -->
    <div class="row">
        <div class="col-lg-12">

            <!-- Card -->
            <div class="card">
                <img class="card-image" src="images/services-icon-1.svg" alt="alternative">
                <div class="card-body">
                    <h4 class="card-title">Security</h4>
                    <p>Our team of enthusiastic marketers will analyse and evaluate how your company stacks against the closest competitors</p>
                </div>
            </div>
            <!-- end of card -->

            <!-- Card -->
            <div class="card">
                <img class="card-image" src="images/services-icon-2.svg" alt="alternative">
                <div class="card-body">
                    <h4 class="card-title">Service</h4>
                    <p>Once the market analysis process is completed our staff will search for opportunities that are in reach</p>
                </div>
            </div>
            <!-- end of card -->

            <!-- Card -->
            <div class="card">
                <img class="card-image" src="images/services-icon-3.svg" alt="alternative">
                <div class="card-body">
                    <h4 class="card-title">Products</h4>
                    <p>With all the information in place you will be presented with an action plan that your company needs to follow</p>
                </div>
            </div>
            <!-- end of card -->

        </div> <!-- end of col -->
    </div> <!-- end of row -->
</div> <!-- end of container -->
</div>




<script src="models/views/landingPage.js"></script>
<?php
include(ROOT_PATH . 'inc/landingPagefooter.php'); ?>