<?php

$pageTitle = "Rebox.ng";
$linkTitle = "about";
require_once($_SERVER["DOCUMENT_ROOT"] . "/constant/config.php");

require_once(ROOT_PATH . 'core/init.php');

include(ROOT_PATH . 'inc/landingPageheader.php');

include(ROOT_PATH . 'inc/landingPageNavbar.php');
?>
<!-- Header -->
<header id="header" class="ex-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1 style="color: #fff;"> About Us</h1>
            </div> <!-- end of col -->
        </div> <!-- end of row -->
    </div> <!-- end of container -->
</header> <!-- end of ex-header -->
<!-- end of header -->

<!-- Privacy Content -->
<div class="ex-basic-2">
    <div class="container">
        <div class="row">
            <div class="col-lg-10 offset-lg-1">
                <div class="text-container">
                    <h3>Rebox</h3>
                    <p style="text-align: justify; font-size: 15px;">Rebox.ng is a property of Business integration and service support , a company duly registered with the Coporate Affairs Commission, Nigeria. Rebox.ng is the platform that gives access to the use of our marketing, Customer Services, Operations and Technology by registered brokers.</p>

                    <div class="row">
                        <div class="col-md-6">
                            <ul class="list-unstyled li-space-lg indent">
                                <li class="media">
                                    <i class="fas fa-square"></i>
                                    <div class="media-body">The geographic area where you use your computer and mobile devices</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-square"></i>
                                    <div class="media-body">Your full name, username, and email address and other contact details</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-square"></i>
                                    <div class="media-body">A unique Evolo user ID (an alphanumeric string) which is assigned to you upon registration</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-square"></i>
                                    <div class="media-body">Other optional information as part of your account profile</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-square"></i>
                                    <div class="media-body">Your IP Address and, when applicable, timestamp related to your consent and confirmation of consent</div>
                                </li>
                                <li class="media">
                                    <i class="fas fa-square"></i>
                                    <div class="media-body">Other information submitted by you or your organizational representatives via various methods</div>
                                </li>
                            </ul>
                        </div> <!-- end of col -->


                    </div> <!-- end of row -->
                </div> <!-- end of text-container-->

                <div class="text-container">
                    <h3>Privacy and Policy</h3>
                    <p>Evolo SaaS Landing Page Template uses visitors' data for the following general purposes:</p>
                    <ol class="li-space-lg">
                        <li>To identify you when you login to your account</li>
                        <li>To enable us to operate the Services and provide them to you</li>
                        <li>To verify your transactions and for purchase confirmation, billing, security, and authentication (including security tokens for communication with installed)</li>
                        <li>To analyze the Website or the other Services and information about our visitors and users, including research into our user demographics and user behaviour in order to improve our content and Services</li>
                        <li>To contact you about your account and provide customer service support, including responding to your comments and questions</li>
                        <li>To share aggregate (non-identifiable) statistics about users of the Services to prospective advertisers and partners</li>
                        <li>To keep you informed about the Services, features, surveys, newsletters, offers, surveys, newsletters, offers, contests and events we think you may find useful or which you have requested from us</li>
                        <li>To sell or market Evolo Landing Page products and services to you</li>
                        <li>To better understand your needs and the needs of users in the aggregate, diagnose problems, analyze trends, improve the features and usability of the Services, and better understand and market to our customers and users</li>
                        <li>To keep the Services safe and secure</li>
                        <li>We also use non-identifiable information gathered for statistical purposes to keep track of the number of visits to the Services with a view to introducing improvements and improving usability of the Services. We may share this type of statistical data so that our partners also understand how often people use the Services, so that they, too, may provide you with an optimal experience.</li>
                    </ol>
                </div> <!-- end of text-container -->

                <div class="text-container">
                    <h3>Security</h3>
                    <p>Evolo is a HTML landing page template tool. By its nature, Services enable our customers to promote their products and services integrate with hundreds of business applications that they already use, all in one place.</p>
                    <p>Services help our customers promote their products and services, marketing and advertising; engaging audiences; scheduling and publishing messages; and analyze the results.</p>
                </div> <!-- end of text container -->

                <div class="text-container">
                    <h3>Partner</h3>
                    <p class="mb-5">By using any of the Services, or submitting or collecting any Personal Information via the Services, you consent to the collection, transfer, storage disclosure, and use of your Personal Information in the manner set out in this Privacy Policy. If you do not consent to the use of your Personal Information in these ways, please stop using the Services.</p>
                </div> <!-- end of text-container -->
            </div> <!-- end of col-->
        </div> <!-- end of row -->
    </div> <!-- end of container -->
</div> <!-- end of ex-basic-2 -->
<!-- end of privacy content -->

<script src="models/views/landingPage.js"></script>
<?php
include(ROOT_PATH . 'inc/landingPagefooter.php'); ?>