<!-- Footer -->
<div class="slider-2">
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="footer-col middle">
                        <h4 style="color:white">DISCLAIMER:</h4>
                        <p style="text-align: justify; font-size: 15px; color:#fff;">Rebox is not a registered broker and we offer based on the registered brokers investment option or advice, investment involves risk, all accounts are traded with the registered broker which include forex.</p>
                    </div>
                </div> <!-- end of col -->
                <div class="col-md-6">
                    <div class="footer-col last">
                        <h4 style="color:white">Social Media</h4>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-facebook-f fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-twitter fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-google-plus-g fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-instagram fa-stack-1x"></i>
                            </a>
                        </span>
                        <span class="fa-stack">
                            <a href="#your-link">
                                <i class="fas fa-circle fa-stack-2x"></i>
                                <i class="fab fa-linkedin-in fa-stack-1x"></i>
                            </a>
                        </span>
                    </div>
                </div> <!-- end of col -->
            </div> <!-- end of row -->
        </div> <!-- end of container -->
    </div> <!-- end of footer -->
    <!-- Copyright -->
    <div class="copyright">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p class="p-small">Copyright © Rebox.ng <a href=""></a></p>
                </div> <!-- end of col -->
            </div> <!-- enf of row -->
        </div> <!-- end of container -->
    </div> <!-- end of copyright -->
    <!-- end of copyright -->
</div>

<!-- end of footer -->

<script src="<?php echo BASE_URL; ?>assets/js/swiper.min.js"></script> <!-- Swiper for image and text sliders -->
<script src="<?php echo BASE_URL; ?>assets/js/jquery.magnific-popup.js"></script> <!-- Magnific Popup for lightboxes -->
<script src="<?php echo BASE_URL; ?>assets/js/scripts.js"></script> <!-- Custom scripts -->

</body>

</html>